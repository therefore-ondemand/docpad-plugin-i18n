# Prepare
I18n = require('i18n-2')

# Export Plugin
module.exports = (BasePlugin) ->

    # Define Plugin
    class i18nPlugin extends BasePlugin
        # Plugin name
        name: 'i18n'

        # Default config.  Can be overridden in plugin settings
        config:
            locales: ['en', 'fr', 'es','de']
            devMode: true
            directory: './locales'
            subdomain: true
            cookie: 'lang'
            readQuery: true
            extension: '.cson'

        internals = {}
        internals.i18n

        extendTemplateData: ({templateData}) ->
            # Prepare
            config = @getConfig()
            templateData._i18n = {}

            # Init i18n-2 library
            config.register = templateData._i18n
            internals.i18n = new I18n(config)

            templateData.i18n = (key, lang) ->
                internals.i18n.setLocale(lang)
                return internals.i18n.__(key)

            templateData.getLocaleName = (lang) ->
                locales = {
                    en: 'English'
                    fr: 'Français'
                    es: 'Español'
                    de: 'German'
                }

                locales[lang]

            templateData.getDocumentLanguage = (document) ->
                document = if document? then document else @document
                language = document.language or 'en'

            templateData.getLanguagePrefix = (document) ->
                document = if document? then document else @document
                language = document.language or 'en'
                prefix = if language is 'en' then '' else language + '/'

            # Chain
            @
